const API_KEY = 'aef59ada40372948713080090d692b82';

const categories = [
    {
        name:'trending',
        title: 'Em alta',
        path: `/trending/movie/week?api_key=${API_KEY}`,
        isLarge: true
    },
    {
        name:'netflixOriginals',
        title: 'Originais Netflix',
        path: `/discover/movie?api_key=${API_KEY}&sort_by=popularity.asc`,
        isLarge: false
    },
    {
        name:'topRated',
        title: 'Populares',
        path: `/tv/top_rated?api_key=${API_KEY}`,
        isLarge: false
    },
    {
        name:'comedy',
        title: 'Comédias',
        path: `/discover/movie?api_key=${API_KEY}&with_genres=35`,
        isLarge: false
    },
    {
        name:'romance',
        title: 'Romances',
        path: `/discover/movie?api_key=${API_KEY}&with_genres=1074`,
        isLarge: false
    },
    {
        name: 'docs',
        title: 'Documentários',
        path: `/discover/movie?api_key=${API_KEY}&with_genres=99`,
        isLarge: false
    }
];
//https://api.themoviedb.org/3/discover/movie?api_key=aef59ada40372948713080090d692b82&with_genres=99

export const getMovies = async (path) => {
    try {
        let url = `https://api.themoviedb.org/3${path}`;
        const response = await fetch(url);
        return await response.json();
    } catch (error) {
        console.log(error);
    }
}

export default categories;