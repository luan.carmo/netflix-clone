import React, { useEffect, useState } from "react";
import { getMovies } from "../../api";
import './style.css'

const imageHost = "https://image.tmdb.org/t/p/original/"
const Row = ({ title, path, isLarge }) => {
    const [movies, setMovies] = useState([]);

    const fetchMovie = async (path) => {
        try {
            const data = await getMovies(path);
            // console.log("dados", data.results)
            setMovies(data?.results);
        } catch (error) {
            console.log(error, 'Row.js');
        }
    }

    useEffect(() => {
        fetchMovie(path)
    }, [path]);

    return(
        <div className="row-container">
            <h2 className="row-header">{title}</h2>

            <div className="row-cards">
                {movies?.map((item) => {
                    return(
                        <img
                            className={`movie-card ${isLarge && "movie-card-large"}`} 
                            key={item.id}
                            src={`${imageHost}${isLarge ? item.backdrop_path : item.poster_path}`}
                            alt={item.name}
                        >
                        </img>
                    );
                })}
            </div>
        </div>
    );
}

export default Row;