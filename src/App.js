import './App.css';
import Row from './components/Row/Row';
import Highlight from './components/Highlight/Highlight';
import Navbar from './components/Navbar/Nav';
import categories from './api';

function App() {
  return (
    <div className="App">
      <Navbar />
      <Highlight />
      {categories.map((category) => {
        return(
          <Row 
            key={category.name} 
            title={category.title} 
            path={category.path}
            isLarge={category.isLarge}
          />
        );  
      })}
    </div>
  );
}

export default App;
